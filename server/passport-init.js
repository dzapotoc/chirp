var LocalStrategy = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');

var User = mongoose.model("User");

module.exports = function(passport) {

  passport.use('signup', new LocalStrategy({
  	  passReqToCallback : true
    },
    function(req, username, password, done) {

      User.findOne({ 'username' : username}, function(err, user) {
      	if(err) {
            console.log('Error in SignUp: ' + err);
            return done(err);
      	}
      	if(user) {
            console.log('User already exists with username: '+username);
            return done(null, false);
      	}

      	var user = new User();
      	user.username = username;
      	user.password = createHash(password);
      	user.save(function(err, user) {
      		if(err) {
      			console.log('Error in Saving user: '+err); 
      			return done(err, false);
      		}
      		console.log('Successfully registered user ' + user.username);
            return done(null, user);
      	});      
      });      
    })
  );

  passport.use('login', new LocalStrategy({
  	  passReqToCallback : true
  	},
  	function(req, username, password, done) {

  	  User.findOne({ 'username' : username}, function(err, user) {
        if (err) {
        	return done(err);
        }
        if (!user) {
        	console.log('User Not Found with username '+username);
			return done(null, false); 
        }

        if (!isValidPassword(user, password)) {
        	console.log('Invalid Password');
			return done(null, false);
        }
        return done(null, user);
  	  });
    })
  );

  passport.serializeUser(function(user, done) {
  	console.log('serializing user: ' , user.username);
  	return done(null, user._id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      console.log('deserializing user:',user.username);
	  done(err, user);
    });
  });


  var isValidPassword = function(user, password) {
  	return bCrypt.compareSync(password, user.password);
  };

  var createHash = function(password) {
  	return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
  };

};
