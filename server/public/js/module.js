var app = angular.module('myApp', ['ngRoute', 'ngResource']).run(function($rootScope, $http) {
  $rootScope.authenticated = false;
  $rootScope.current_user = '';

  $rootScope.logout = function() {
    $http.get('/auth/signout');

    $rootScope.authenticated = false;
    $rootScope.current_user = '';
  };
});

app.config(function($routeProvider) {

  $routeProvider
    
    .when('/', {
      templateUrl: 'main.html',
      controller: 'myCtrl'
    })

    .when('/login', {
      templateUrl: 'login.html',
      controller: 'authCtrl'
    })

    .when('/register', {
      templateUrl: 'register.html',
      controller: 'authCtrl'
    });
});

app.factory('postService', function($resource) {
  return $resource('/api/posts/:id');
});

app.controller('myCtrl', function($scope, $rootScope, postService) {
  $scope.posts = postService.query();
  $scope.newPost = {username: '', text: '', created_at: ''};

  $scope.post = function() {
  	$scope.newPost.created_at = Date.now();
    $scope.newPost.username = $rootScope.current_user;
    postService.save($scope.newPost, function() {
      $scope.newPost = {username: '', text: '', created_at: ''};
      $scope.posts = postService.query();
    }); 
  };

});

app.controller('authCtrl', function($scope, $rootScope, $http, $location) {
  $scope.user = {username: '', password: ''};
  $scope.error_message = '';

  $scope.login = function() {
  	$http.post('/auth/login', $scope.user).success(function(data) {
      $rootScope.authenticated = true;
      $rootScope.current_user = data.user.username;

      $location.path('/');
    });
  };

  $scope.register = function() {
  	$http.post('/auth/signup', $scope.user).success(function(data) {
      $rootScope.authenticated = true;
      $rootScope.current_user = data.user.username;

      $location.path('/');
    });
  };

});